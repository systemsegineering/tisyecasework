\section{Document Overview}
This Preliminary Interface Control Document (PICD) describes the initial interface characteristics for the Self-protection Suite.
Abbreviations used in this document are described in \autoref{tab:abb-picd}.

\begin{table}[!htpb] \centering
	\begin{tabular}{| l | l | l |}
		\hline
		Abbreviation & Name \\
		\hline
		AMC & Aircraft Mission Computer \\
		CCU & Cockpit Control Unit \\
		DSS & Digital Sequence Switch \\
		FDR & Flight Data Recorder \\
		HWI & Hardware Interlock \\
		IBD & Internal Block Definition \\
		IF  & Interface \\
		ICS & Intercommunication System \\
		MWS & Missile Warning System \\
		STM & Standard Magazines \\
		\hline
	\end{tabular}
	\caption{Abbreviations used in this document}
	\label{tab:abb-picd} 
\end{table}

\bib % referenced documents

\section{Interface Design}
 This section describes the interface design for self-protection suite.

\subsection{Interface Identification and Diagrams}
The interfaces are summarized in \autoref{tab:interfaces}.\\ 
The interfaces are divided into two categories:
\begin{itemize}
\item Electrical interfaces (EIF)
\item Physical interfaces (PIF)
\end{itemize}
 \begin{table}[!htpb] \centering
 	\begin{tabular}{| l | l | l |}
 		\hline
 		Identifier & Name & Interfacing Components \\
 		\hline
 		EIF1 & MIL-DB & POD, ICS, CCU, AMC, DSS, MWS \\
 		\hline
 		EIF2 & 28VDC & PCU, DSS \\
 		\hline
 		EIF3 & 115VAC & POD, PCU \\
 		\hline
 		EIF4 & Sensor Data & HWI \\
 		\hline
 		EIF5 & Plane Grounded & HWI, DSS \\
 		\hline
 		EIF6 & Trigger Event & DSS, STM \\
 		\hline
 		EIF7 & ARINC 664 & CCU. FDR \\
 		\hline
 		PIF1 & User Input & CCU \\
 		\hline
  		PIF2 & Pod Plug Interface & POD \\
  		\hline
 		PIF3 & Cockpit Control Unit Interface & CCU \\
 		\hline
 	\end{tabular}
 	\caption{Interface identification table}
 	\label{tab:interfaces} 
 \end{table}

All components in \autoref{tab:interfaces} are described in the PDD\cite{pdd12}.\\
The interfaces for the self-protection suite are described in \autoref{fig:ibd-sps}.
\begin{figure}[ht]
	\centering
	\includegraphics[width=0.9\textwidth, clip=true,trim=17 17 17 17]{figs/ibd-protectionsuite.pdf}
	\caption{Ibd diagram for the self-protection suite}
	\label{fig:ibd-sps}
\end{figure}

The pod's internal interfaces are further described in \autoref{fig:ibd-pod}, since it is a subsystem with many internal interfaces.
\begin{figure}[ht]
	\centering
	\includegraphics[width=0.9\textwidth, clip=true,trim=17 17 17 17]{figs/ibd-pod.pdf}
	\caption{Ibd diagram for the pod}
	\label{fig:ibd-pod}
\end{figure}

\FloatBarrier

\subsection{Interface Descriptions}

\subsubsection{Electrical Interfaces}

\paragraphnl{EIF1 - MIL-DB}
It is a military data bus standard (MIL-STD-1553B).
It is used for data communication between different components which are described in \autoref{tab:interfaces}.
The standard is briefly described in SEMP\cite{semp12}.
The \textbf{physical layer} of the databus consists of a wire pair with ~80 $\Omega$ impedance at 1 MHz.
The bus' bit rate is 1 Mbps, and it is isolated so that it reduces impact of a short circuit and assures that no current is conducted through the self-protection suite.
The \textbf{bus protocol} is a multiplex databus system which consists of a Bus Controller (BT) controlling multiple Remote Terminals (RT).
The bus is based on message transmission.
There are 10 different message types (information transfer formats) in total, which are shown in \autoref{fig:mil-messages}.
\begin{figure}[!htbp]
	\centering
	\includegraphics[width=\textwidth]{figs/mil-messages.pdf}
	\caption{Information Transfer Formats. Source: www.ddc-web.com/Documents/dguidehg.pdf}
	\label{fig:mil-messages}
\end{figure}
For the self-protection suite the most used message types are the \textit{controller to RT transfer}, \textit{RT to controller transfer} and \textit{RT to RT transfer}.
Usage of the command word bits and status word bits is shown in \autoref{table:mil-bits}.
\begin{table}[!htbp]
	\begin{tabular}{ |p{0.25\textwidth}|p{0.25\textwidth}|p{0.25\textwidth}|p{0.25\textwidth}| }
		\hline
		\multicolumn{4}{|c|}{Command word bit usage (16 bits)} \\
		\hline
		RT address (bit 1-5) & Receive/transmit (bit 6) & Location of data (bit 7-11) & Number of words to expect (bit 12-16) \\
		\hline
	\end{tabular}
	\begin{center}
		\begin{tabular}{ |l|l| }
			\hline
			\multicolumn{2}{|c|}{Status word bit usage (16 bits)} \\
			\hline
			RT address (bit 1-5) & Single bit condition codes (bit 6-16)\\
			\hline
		\end{tabular}
	\end{center}
	\caption{Command and status word bits}
	\label{table:mil-bits}
\end{table}
For more detailed information please look up the standard since the self-protection suite complies with the MIL-STD-1553B standard.

\paragraphnl{EIF2 - 28VDC}
It is the power supply from the PCU converted from 115VAC to 28VDC.
It consists of two terminals, plus and minus

\paragraphnl{EIF3 - 115VAC, 400Hz}
It is the power supply from the plane which shall power the pod.
It consists of two terminals phase and neutral.

\paragraphnl{EIF4 - Sensor Data}
It is a measurement of the approximate distance to the ground by using a proximity sensor.
This interface uses the \textbf{S}imple \textbf{S}ensor \textbf{I}nterface protocol (SSI).
It uses point-to-point SSI and SSI/UART as medium for messaging.

\paragraphnl{EIF5 - Plane Grounded}
It is a signal which indicates that the plane is grounded and that the switches cannot be switched on.
This signal is generated when an event occurs from the proximity sensor as high (1), using TTL logic voltage levels.


\paragraphnl{EIF6 - Trigger Event}
It is a signal that activates the dispense of flares and chaffs from the magazines.
This signal is generated when an event of plane grounded occurs from the HWI.
It generates a high (1) signal, using TTL logic voltage levels.

\paragraphnl{EIF7 - ARINC 664}
It is a standard that defines a deterministic Ethernet network which is used as an avionic data bus.
It is used for data communication between different components which are described in \autoref{tab:interfaces}.
The data that is exchanged over the bus is: data and voice that can be used for ground inspection after exercise or combat.
A specific implementation of the standard is ``Avionics Full-Duplex Switched Ethernet'' (AFDX) which is based on COTS ethernet technology.
It uses full duplex and critical traffic is prioritized using QoS policies.
The physical layer of the databus consists of a twisted par or fiber optic cables.
As the standard uses ethernet as data-link layer, transmission rates of up to 1Gbps is possible.
ARINC 664 uses the ``standard'' ethernet header with specific ARINC 664 MAC addresses and UDP/IP as transport-layer protocol.
This can be seen in \autoref{fig:afdx-ip}.
\begin{figure}[!htbp]
	\centering
	\includegraphics[width=0.6\textwidth]{figs/afdx.png}
	\caption{AFDX Frame}
	\label{fig:afdx-ip}
\end{figure}
For more information see the standard, as the SPS is in compliance with it.

\subsubsection{Physical Interfaces}

\paragraphnl{PIF1 - User Input}
The user interface can be seen in \autoref{fig:ccu_physical_interface}.
The user can change between three different modes as described in \cite{srs14}, via a 3-mode switch.
For each mode there is a corresponding green diode that shines when the mode is activated.
If the pod is in either manual or semi-automatic mode, the red ``fire'' button can be used to dispense flares and chaffs.
In automatic mode, the button is non-functional. 
\begin{figure}[!htbp]
	\centering
	\includegraphics[width=0.3\textwidth]{../pdd/content/cockpit-ctrl-unit.png}
	\caption{Cockpit control unit physical interface}
	\label{fig:ccu_physical_interface}
\end{figure}

\paragraphnl{PIF2 - Pod Plug Interface}
The pod is attached to the air-plane wing with a standard t-hook as seen in \autoref{fig:pod_physical_t_hook_interface}, where the t-hook is of dark blue color, and the pod of light blue.

The pod is supplied with electrical connections such as data buses and power supply by a standard male CEE plug attached to the t-hook.
There are 6 wires in total, two for the power supply, and four for the redundant data bus (2x2 wires).
The power supply cables is of size $1.5\text{mm}^2$ as the pod's supply is 115AC and can use up to 700W.
The T-hook and plug can be seen in \autoref{fig:pod_physical_t_hook_interface}.
The plug is connected to a female CEE plug when the pod is attached correctly.

\begin{figure*}[ht!]
    \centering
    \begin{subfigure}[t]{0.5\textwidth}
	 \centering
	 \includegraphics[width=1.0\textwidth]{../pdd/content/t-hook-detail.png}
	 \caption{Pod physical plug interface}
	 \label{fig:pod_physical_plug_interface}
    \end{subfigure}%
    ~ 
    \begin{subfigure}[t]{0.5\textwidth}
	 \centering
	 \includegraphics[width=1.0\textwidth]{../pdd/content/t-hook-wing.png}
	 \caption{Pod physical t-hook interface}
	 \label{fig:pod_physical_t_hook_interface}
    \end{subfigure}
    \caption{Physical pod interfaces}
\end{figure*}

\paragraphnl{PIF3 - Cockpit Control Unit Interface}

The cockpit control unit is mounted on the plane using ``Redi-Rail Roller Bearing Linear Guide System''. The unit is ``slided'' onto the rail system until it is locked by a hardware mechanism.
The rail on the cockpit control unit (left side) can be seen in \autoref{fig:ccu_physical_interface}.
There is a mounting on each side of CCU to be able to fasten it efficiently on the rail system

\subsection{Requirements Traceability}
The interfaces are traced to the requirements by using a requirement verification traceability matrix\cite{rvtm11}.

