%!TEX root = ../master.tex
\input{content/doc-overview}
\bib
\section{System-wide Design Decisions}
This section provides system-wide decisions, including design of the pod structure, financial, weight, and power budgets, as well as inputs, outputs, and actions. \\
\input{content/podstructure.tex}
\input{content/power-budget.tex}
\input{content/weight-budget.tex}
\input{content/budget.tex}
%
\subsection{Inputs}
\begin{description}
    \item [Confirm/Deny suggested action (ID: I/O-CDA)] Confirm/Deny suggested action is used to confirm/deny actions in all modes that require user intervention.
    \item [Change mode (ID: I/O-CM)] It should be possible to switch between the modes described in \cite{srs15}.
    \item [Incoming missile (ID: I/O-IM)] An Incoming missile event occurs when the MWS detects an incoming missile.
    \item [Specify pattern (ID: I/O-SP)] is the input that allow for different patterns of payload dispersion to be specified, such that more missile threats can be overcome safely.
\end{description}
\subsection{Outputs}
\begin{description}
    \item [Audio warning (ID: I/O-AW)] Audio warnings are produced when incoming threats happen. The warnings occur regardless of mode.
    \item [Status information (ID: I/O-SI)] Status information are presented for all inputs and outputs of SPS.
    \item [Chaffs and flares (ID: I/O-CAF)] The dispersion of chaffs and flares is the desired output, when necessary for dealing with incoming missile threats.
\end{description}
\subsection{Actions}
The general flow of actions during missile threats is illustrated in \cref{fig:fc1}.
\begin{figure}[!htbp]
	\centering
	\includegraphics[width=0.5\textwidth]{content/fc1.pdf}
	\caption{Flowchart [ID: FC-1] Shows the basic flow of actions during a missile threat.}
	\label{fig:fc1}
\end{figure}
\subsubsection{Flare Patterns}\label{sec:flares}
The system is able to dispense flares in many different patterns. \Cref{fig:flares_top} and \cref{fig:flares_front} shows which directions the flares can be dispensed in. A flare pattern is described by a sequence of flare directions and time between dispenses. An example of a flare pattern is shown in \cref{fig:flare_prog}. \\ \\
For automatic mode\cite{srs15} the SPS must be able to determine an appropriate flare pattern without user intervention.
This can be accomplished with estimators(such as Kalman filters) that use a series of missile positions to predict its trajectory, as shown in \autoref{fig:predict}.
The trajectory is used to choose which magazine is dispensed when, such that the incoming missile and the flares collide, as shown in \autoref{fig:avoid}.
\begin{figure}[!htbp]
	\centering
	\includegraphics[width=0.5\textwidth]{content/prediction.png}
	\caption{Estimation of missile and flare trajectory.}
	\label{fig:predict}
\end{figure}
\begin{figure}[!htbp]
	\centering
	\includegraphics[width=0.5\textwidth]{content/flare-hit.png}
	\caption{Flare eliminating missile threat.}
	\label{fig:avoid}
\end{figure}
\begin{figure}[!htbp]
	\centering
	\includegraphics[width=0.5\textwidth]{content/flares-top.png}
	\caption{Flare directions seen from the top of an F-16.}
	\label{fig:flares_top}
\end{figure}
\begin{figure}[!htbp]
	\centering
	\includegraphics[width=0.5\textwidth]{content/flares-front.png}
	\caption{Flare directions sheen from the front of an F-16.}
	\label{fig:flares_front}
\end{figure}
\begin{figure}[!htbp]
	\centering
	\includegraphics[width=0.7\textwidth]{content/flare_prog.png}
	\caption{Example of a flare pattern.}
	\label{fig:flare_prog}
\end{figure}
\subsection{Securing Electronics}
Since a F-16 is a rather hostile environment for electronics, special care must be taken to protect and secure it against the vibrations and g-forces produced by the aeroplane.
To protect against the extreme forces, all circuit boards will be reinforced, which will make them thicker.
The extra size will be minimised from the use of surface-mount technology. \\
All circuit boards will be mounted with thick rubber rings, to counteract the vibrations.
\section{System Architectural Design}
This section describes the systems architectural design. It is split into two parts namely system components and concept of execution.
\subsection{System Components} 
The system can be broken down into the components shown in \cref{fig:bdd}. The purpose of each component is described below.
\begin{figure}[!htbp]
	\centering
	\includegraphics[width=1\textwidth]{content/bdd.pdf}
	\caption{Block definition diagram for the SPS.}
	\label{fig:bdd}
\end{figure}
\begin{description}
	\item[Pod (ID: POD)] The pod is a container that is mounted underneath the left hand wing on the F-16. It contains the MWS and the equipment necessary to dispense 8 standard magazines. It can withstand the forces and temperatures it will be under, during flight.
	The POD's power consumption cannot exceed 700W \cite{srs15}.
	The construction of the Pod is subcontracted. See \cref{sec:pod-dd} for detailed design decisions regarding the pod.
	\item[Cockpit Control Unit (ID: CCU)] The cockpit control unit is a special purpose computer to be mounted inside the cockpit. It serves as the interface between the co-pilot and the SPS. The CCU require development of electronics and software. \\ \\
    The CCU structure will be made of aluminium and mounted with the rail system provided in the cockpit, see \cref{fig:ccu}.
    \begin{figure}[!htbp]
    	\centering
    	\includegraphics[width=0.5\textwidth]{content/cockpit-ctrl-unit.png}
    	\caption{The mounting rails of the CCU is placed on each side of the CCU.}
    	\label{fig:ccu}
    \end{figure}
    1) It can be swapped easily and 2) It does not need to be fastened because it does not become loose due to vibrations, like bolts do.
    Since it is critical that the software never malfunctions, it will be written in Haskell, such that the implementation is as close to the formal specifications.
    %
	\item[Intercom System (ID: ICY)] The intercom system enables communication between the pilots of the F-16. Further it is the communication center of the air craft. Its text-to-speech engine is used in the SPS to provide audio clues informing the pilots about detected threats. The ICY is a component that already exists in F-16, and require no development effort.
	\item[Aircraft Mission Computer (ID: AMC)] The Aircraft Mission Computer is the system that provides the pilots with situational awareness and combat system control. The SPS utilizes the AMC to provide the pilots with SPS status information such as friendly F-16's threat status. And test results from the SPS. The AMC is a component that already exists in F-16s, and require no development effort.
    \item[Missile Warning System (ID: MWS)] The Missile Warning System contain six sensors and an electronic control unit, which is used to recognize threats from incoming missiles. The MWS is GFE and therefore requires no development effort.
    The MWS requires a maximum of 85W from 28VDC.
    It is sketched in \cref{fig:mws}.
    \begin{figure}[!htbp]
    	\centering
    	\includegraphics[width=0.3\textwidth]{content/mws.png}
    	\caption{The missile warning system provided to the project.}
    	\label{fig:mws}
    \end{figure} \\ \\
    The MWS is made programmable by the manufacturer, which have provided it with a USB-port and a switch, that needs to be in the program position when booting.
    If the switch is set when booting, the MWS will read the binary named \texttt{os.bin} from the USB, and override the current file on the internal flash ram.
    This procedure causes the new binary to be loaded at next reboot.
    %
    \item[Standard Magazine (ID: STM)] A standard magazine is a magazine containing chaffs and flares, that can dispense these in a configurable pattern. STMs are meant for one time use, and thus shall be replaceable. Magazines are composed of a hardware casing and some electronics. It is sketched in \cref{fig:stm}.
    \begin{figure}[!htbp]
    	\centering
    	\includegraphics[width=0.3\textwidth]{content/stm.png}
    	\caption{Standard magazine. The electronics are housed in the rear end along with power connections.}
    	\label{fig:stm}
    \end{figure}
    \item[Power Control Unit (ID: PCU)] The Power Control Unit is responsible for converting power from the level the F-16 provides to what the different components require\cite{srs15}. The PCU is COTS, consisting of a hardware casing and electronics.
    It can output a maximum of 250W \cite{srs15} and is sketched in \cref{fig:pcu}.
    \begin{figure}[!htbp]
    	\centering
    	\includegraphics[width=0.4\textwidth]{content/pcu.png}
    	\caption{The power control unit.}
    	\label{fig:pcu}
    \end{figure}
    \item[Digital Sequencer Switches (ID: DSS)] Digital Sequencer Switches ignite chaffs and flares from the STMs and dispense them in the configured pattern.
    They consume 3W per DSS and are COTS circuit boards, see \cref{fig:dss}.
    \begin{figure}[!htbp]
    	\centering
    	\includegraphics[width=0.3\textwidth]{content/dss.png}
    	\caption{The DSS have two outputs (placed opposite to each other) and an input.}
    	\label{fig:dss}
    \end{figure} \\ \\
    To cause the DSS to ignite its chaffs and flares, a package containing the identifier of one of the connected magazines, is all that is required.
    Each DSS has two unique identifiers, such that a magazine cannot be ignited accidentally, but sending a correct identifier to a wrong DSS.
    Since the bus used for communication with the CCU\cite{srs15}, provides real-time guarantees, the approach described above is sufficient for generating all patterns.
    % what interface, how to write/comm
    \item[Structure (ID: STR)] The structure is a physical container that contains all the pod's internal components.
    The pod structure development has been sub-contracted to ``Company F'' at a cost of 500.000kr.
    The SRS can be seen in \cite{cf-srs11} and the PDD in \cite{cf-pdd10}.
    \item[Flight Data Recorder (ID: FDR)] The Flight Data Recorder is a component that all aeroplanes are equipped with, to ease investigations into crashes. The FDRs implement the ARINC 667 standard. The FDR is a component that already exists in the F-16, and require no development effort. 
\end{description}
\subsection{Concept of Execution}
This section contains sequence diagrams that illustrates important dynamic behaviours of the SPS. \cref{fig:sd1} shows the sequence diagram for scenario 1 described in the Concepts of Operations \cite{conops12}. \Cref{fig:sd3} shows the dynamics happening when receiving a zeroize signal.
\begin{figure}[h]
	\centering
	\includegraphics[width=1\textwidth]{content/sd1.pdf}
	\caption{Sequence diagram of dynamics regarding incoming missile using semi-automatic mode.}
	\label{fig:sd1}
\end{figure}

\begin{figure}[!htbp]
	\centering
	\includegraphics[width=1\textwidth]{content/sd2.pdf}
	\caption{Sequence diagram of dynamics regarding incoming zeroize signal.}
	\label{fig:sd3}
\end{figure}
\subsection{Interface Design}
The interface design is to be found in the Preliminary Interface Control Document \cite{icd10}.
\subsection{Engineering Capabilities}
Since SPS is a critical system that is concerned with the safety of pilots, mainly senior engineers will be working on the project. All of whom have experience with embedded real-time systems. A split of 60/40 between software and hardware engineers will be working on SPS. All of the engineers have experience with systems used on military aeroplanes. Two of the engineers are experts in the field of communication systems, and one is a former F-16 engineer for ten years.
\section{Requirements Traceability}
The traceability from each system component to the system requirements is available in the Requirements Verification Traceability Matrix \cite{rvtm11}.
