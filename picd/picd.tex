\section{Document Overview}
This Preliminary Interface Control Document (PICD) describes the initial interface characteristics for the Self-protection Suite.
Abbreviations used in this document are described in \autoref{tab:abb-picd}.

\begin{table}[!htpb] \centering
	\begin{tabular}{| l | l | l |}
		\hline
		Abbreviation & Name \\
		\hline
		AMC & Aircraft Mission Computer \\
		CCU & Cockpit Control Unit \\
		DSS & Digital Sequence Switch \\
		FDR & Flight Data Recorder \\
		HWI & Hardware Interlock \\
		IBD & Internal Block Definition \\
		IF  & Interface \\
		ICS & Intercommunication System \\
		MWS & Missile Warning System \\
		STM & Standard Magazines \\
		\hline
	\end{tabular}
	\caption{Abbreviations used in this document}
	\label{tab:abb-picd} 
\end{table}

\bib % referenced documents

\section{Interface Design}
 This section describes the interface design for self-protection suite.

\subsection{Interface Identification and Diagrams}
The interfaces are summarized in \autoref{tab:interfaces}.\\ 
The interfaces are divided into two categories:
\begin{itemize}
\item Electrical interfaces (EIF)
\item Physical interfaces (PIF)
\end{itemize}
 \begin{table}[!htpb] \centering
 	\begin{tabular}{| l | l | l |}
 		\hline
 		Identifier & Name & Interfacing Components \\
 		\hline
 		EIF1 & MIL-DB & POD, ICS, CCU, AMC, DSS, MWS \\
 		\hline
 		EIF2 & 28VDC & PCU, DSS \\
 		\hline
 		EIF3 & 115VAC & POD, PCU \\
 		\hline
 		EIF4 & Sensor Data & HWI \\
 		\hline
 		EIF5 & Plane Grounded & HWI, DSS \\
 		\hline
 		EIF6 & Trigger Event & DSS, STM \\
 		\hline
 		EIF7 & ARINC 664 & CCU \\
 		\hline
 		PIF1 & User Input & CCU \\
 		\hline
  		PIF2 & Pod Plug Interface & POD \\
  		\hline
 		PIF3 & Cockpit Control Unit Interface & CCU \\
 		\hline
 	\end{tabular}
 	\caption{Interface identification table}
 	\label{tab:interfaces} 
 \end{table}

All components in \autoref{tab:interfaces} are described in the PDD\cite{pdd12}.\\
The interfaces for the self-protection suite are described in \autoref{fig:ibd-sps}.
\begin{figure}[ht]
	\centering
	\includegraphics[width=0.9\textwidth, clip=true,trim=17 17 17 17]{figs/ibd-protectionsuite.pdf}
	\caption{Ibd diagram for the self-protection suite}
	\label{fig:ibd-sps}
\end{figure}

The pod's internal interfaces are further described in \autoref{fig:ibd-pod}, since it is a subsystem with many internal interfaces.
\begin{figure}[ht]
	\centering
	\includegraphics[width=0.9\textwidth, clip=true,trim=17 17 17 17]{figs/ibd-pod.pdf}
	\caption{Ibd diagram for the pod}
	\label{fig:ibd-pod}
\end{figure}

\FloatBarrier

\subsection{Interface Descriptions}

\subsubsection{Electrical Interfaces}

\paragraphnl{EIF1 - MIL-DB}
It is a military data bus standard (MIL-STD-1553B).
It is used for data communication between different components which are described in \autoref{tab:interfaces}.
The standard is briefly described in SEMP\cite{semp12}.

\paragraphnl{EIF2 - 28VDC}
It is the power supply from the PCU converted from 115VAC to 28VDC.
It consists of two terminals, plus and minus

\paragraphnl{EIF3 - 115VAC, 400Hz}
It is the power supply from the plane which shall power the pod.
It consists of two terminals phase and neutral.

\paragraphnl{EIF4 - Sensor Data}
It is a measurement of the approximate distance to the ground.

\paragraphnl{EIF5 - Plane Grounded}
It is a signal which indicates that the plane is grounded and that the switches cannot be switched on.

\paragraphnl{EIF6 - Trigger Event}
It is a signal that activates the dispense of flares and chaffs from the magazines.

\paragraphnl{EIF7 - ARINC 664}
A standard that defines a deterministic Ethernet network which is used as an avionic data bus.
\subsubsection{Physical Interfaces}

\paragraphnl{PIF1 - User Input}
It consists of the chosen mode and whether the ``fire'' button has been activated (can be seen in \autoref{fig:ccu_physical_interface})

\paragraphnl{PIF2 - Pod Plug Interface}
The pod is attached to the air-plane wing with a t-hook as seen in \autoref{fig:pod_physical_t_hook_interface}, where the t-hook is of dark blue color, and the pod of light blue.

The pod is supplied with electrical connections such as data buses and power supply by a standard male CEE plug attached to the t-hook.
This can be seen on \autoref{fig:pod_physical_t_hook_interface}.
The plug is connected to a female CEE plug when the pod is attached correctly.

\begin{figure*}[ht!]
    \centering
    \begin{subfigure}[t]{0.5\textwidth}
	 \centering
	 \includegraphics[width=1.0\textwidth]{../pdd/content/t-hook-detail.png}
	 \caption{Pod physical plug interface}
	 \label{fig:pod_physical_plug_interface}
    \end{subfigure}%
    ~ 
    \begin{subfigure}[t]{0.5\textwidth}
	 \centering
	 \includegraphics[width=1.0\textwidth]{../pdd/content/t-hook-wing.png}
	 \caption{Pod physical t-hook interface}
	 \label{fig:pod_physical_t_hook_interface}
    \end{subfigure}
    \caption{Physical pod interfaces}
\end{figure*}

\paragraphnl{PIF3 - Cockpit Control Unit Interface}

The cockpit control unit is mounted on the plane using a ``rail'' system. The unit is ``slided'' onto the rail system until it is locked by a hardware mechanism.
The rail on the cockpit control unit (left side) can be seen in \autoref{fig:ccu_physical_interface}.

\begin{figure}[!htbp]
 \centering
 \includegraphics[width=0.4\textwidth]{../pdd/content/cockpit-ctrl-unit.png}
 \caption{Cockpit control unit physical interface}
 \label{fig:ccu_physical_interface}
\end{figure}

The user interface can also be seen in \autoref{fig:ccu_physical_interface}.
The user can change between three different modes as described in \cite{srs15}, via a 3-mode switch.
For each mode there is a corresponding diode that shines when the mode is activated.
If the pod is in either manual or semi-automatic mode, the red ``fire'' button can be used to dispense flares and chaffs.
In automatic mode, the button is non-functional. 

\subsection{Requirements Traceability}
The interfaces are traced to the requirements by using a requirement verification traceability matrix\cite{rvtm11}.

